import { HashStorage } from './HashStorage.js';

const coctailsStorage = new HashStorage();



const addButton = document.getElementById('add-coctail');
addButton.onclick = function () {
  const name = window.prompt('Введите название напитка');
  const isAlcohol = window.prompt('Напиток алкогольный?');
  const ingred = window.prompt('введите составляющие');
  const recipe = window.prompt('введите рецепт');
  coctailsStorage.add(name, {name, isAlcohol, ingred, recipe});
  console.log('Coctail was added', coctailsStorage.getValue(name));
}

const deleteButton = document.getElementById('delete-coctail');
deleteButton.onclick = function () {
  const name = window.prompt('Введите название напитка');
  coctailsStorage.deleteValue(name);
  console.log('Coctail ' + name + ' was deleted');
  
}

const getButton = document.getElementById('get-coctail');
getButton.onclick = function () {
  const name = window.prompt('Введите название напитка');
  coctailsStorage.getValue(name);
  console.log(coctailsStorage.getValue(name));
  const a = coctailsStorage.getValue(name);
  document.getElementById("recipe").innerHTML = 'Содержит алкоголь: ' + a.isAlcohol;
  document.getElementById("recipe1").innerHTML = 'Ингредиенты: ' + a.ingred;
  document.getElementById("recipe2").innerHTML = 'Рецепт: ' + a.recipe;
  console.log(a); 
}

const listButton = document.getElementById('all-coctails');
listButton.onclick = function () {
  coctailsStorage.getKeys();
  console.log('Coctails list: ' + coctailsStorage.getKeys());
  const a = coctailsStorage.getKeys();
  document.getElementById("list").innerHTML = Object.values(a);
}

coctailsStorage.add('Бульвардье',{isAlcohol: 'алкогольный', ingred: 'несколько кубиков льда, 30 мл кампари, 30 мл красного сладкого вермута', recipe: 'смешать'}); 
coctailsStorage.add('Виски сауэр',{isAlcohol: 'алкогольный', ingred: 'несколько кубиков льда, 45 мл бурбона, 30 мл лимонного сока', recipe: 'смешать'});
coctailsStorage.add('Чай',{isAlcohol: 'безалкогольный', ingred: 'вода, кипяток', recipe: 'залить заварку кипятком'});


